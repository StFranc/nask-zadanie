﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Linq;
using NASK_Zadanie.Models;
using TikaOnDotNet.TextExtraction;
using HtmlAgilityPack;

namespace NASK_Zadanie.Services
{
	public class FileDAO
	{
		private static readonly List<string> extensions
			= new List<string> { ".doc", ".docx", "odt", ".rtf", ".pdf" };

		static readonly String filesLocation
			= System.Web.Hosting.HostingEnvironment.MapPath("~/UploadedFiles");

		public void SaveFile(FileUploadVM model)
		{
			Directory.CreateDirectory(filesLocation);

			string fileName = Path.GetFileName(model.File.FileName);
			string path = Path.Combine(filesLocation, fileName);
			try
			{
				// doc, docx, odt, rtf, pdf
				if (extensions.Any(ext => model.File.FileName.EndsWith(ext)))
				{
					if (model.File.ContentLength > 0)
					{

						// saving file
						model.File.SaveAs(path);

						// read file with TikaOnDotNet
						var textExtractor = new TextExtractor();
						var fileContents = textExtractor.Extract(path);
					}
				}
				// html
				else if (model.File.FileName.EndsWith(".html"))
				{
					BinaryReader b = new BinaryReader(model.File.InputStream);
					byte[] binData = b.ReadBytes(model.File.ContentLength);
					b.Close();

					string html = System.Text.Encoding.UTF8.GetString(binData);
					var htmlDoc = new HtmlDocument();
					htmlDoc.LoadHtml(html);
					var content = htmlDoc.GetElementbyId("content").ToString();
					File.WriteAllText(path, content);
				}

				// saving metadata??
				new XDocument(new XElement("FileMetadata",
					new XElement[] {
								new XElement("Author", model.Author),
								new XElement("Title", model.Title),
								new XElement("Description", model.Description)
					})).Save(path + ".xml");

			}
			catch (Exception ex)
			{
				throw;
			}
		}

		public List<FileData> GetFiles()
		{
			Directory.CreateDirectory(filesLocation);

			var files = Directory.GetFiles(filesLocation).Where(f => f.EndsWith(".xml"));
			List<FileData> filesData = new List<FileData>();
			foreach(string f in files)
			{
				var xmlDoc = new XmlDocument();
				xmlDoc.Load(f);
				var node = xmlDoc.SelectSingleNode("/FileMetadata");
				var fileData = new FileData
				{
					Author = node["Author"].InnerText,
					Title = node["Title"].InnerText,
					Description = node["Description"].InnerText,
				};
				var textExtractor = new TextExtractor();
				var fileContents = textExtractor.Extract(f.Remove(f.Length - 4));

				fileData.Content = fileContents.Text;
				filesData.Add(fileData);
			}

			return filesData;
		}
	}
}