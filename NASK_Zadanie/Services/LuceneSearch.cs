﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Lucene.Net.Analysis.Standard;
using Lucene.Net.Documents;
using Lucene.Net.Index;
using Lucene.Net.Search;
using Lucene.Net.Store;
using NASK_Zadanie.Models;

namespace NASK_Zadanie.Services
{
	public static class LuceneSearch
	{
		private static string _luceneDir =
			Path.Combine(HttpContext.Current.Request.PhysicalApplicationPath, "lucene_index");

		private static FSDirectory _directoryTemp;

		private static FSDirectory _directory
		{
			get
			{
				if (_directoryTemp == null) _directoryTemp = FSDirectory.Open(new DirectoryInfo(_luceneDir));
				if (IndexWriter.IsLocked(_directoryTemp)) IndexWriter.Unlock(_directoryTemp);
				var lockFilePath = Path.Combine(_luceneDir, "write.lock");
				if (File.Exists(lockFilePath)) File.Delete(lockFilePath);
				return _directoryTemp;
			}
		}

		private static void _addToLuceneIndex(FileData sampleData, IndexWriter writer)
		{
			// remove older index entry
			/*var searchQuery = new TermQuery(new Term("Id", sampleData.Id.ToString()));
			writer.DeleteDocuments(searchQuery);*/

			// add new index entry
			var doc = new Document();

			// add lucene fields mapped to db fields
			doc.Add(new Field("Title", sampleData.Title, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.Add(new Field("Description", sampleData.Description, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.Add(new Field("Author", sampleData.Author, Field.Store.YES, Field.Index.NOT_ANALYZED));
			doc.Add(new Field("Content", sampleData.Author, Field.Store.YES, Field.Index.NOT_ANALYZED));

			// add entry to index
			writer.AddDocument(doc);
		}

		public static void AddUpdateLuceneIndex(IEnumerable<FileData> sampleDatas)
		{
			// init lucene
			var analyzer = new StandardAnalyzer(Lucene.Net.Util.Version.LUCENE_30);
			using (var writer = new IndexWriter(_directory, analyzer, IndexWriter.MaxFieldLength.UNLIMITED))
			{
				// add data to lucene search index (replaces older entry if any)
				foreach (var sampleData in sampleDatas) _addToLuceneIndex(sampleData, writer);

				// close handles
				analyzer.Close();
				writer.Dispose();
			}
		}

		public static void AddUpdateLuceneIndex(FileData sampleData)
		{
			AddUpdateLuceneIndex(new List<FileData> { sampleData });
		}

		private static FileData _mapLuceneDocumentToData(Document doc)
		{
			return new FileData
			{
				Title = doc.Get("Title"),
				Author = doc.Get("Author"),
				Description = doc.Get("Description"),
				Content = doc.Get("Content")
			};
		}
	}
}