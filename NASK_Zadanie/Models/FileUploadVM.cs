﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NASK_Zadanie.Models
{
	public class FileUploadVM
	{
		[Required]
		public HttpPostedFileBase File { get; set; }

		[Required]
		public string Title { get; set; }

		[Required]
		public string Author { get; set; }

		[Required]
		public string Description { get; set; }
	}
}