﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace NASK_Zadanie.Models
{
	public class FileData
	{
		//public int Id { get; set; }

		public string Title { get; set; }

		public string Author { get; set; }

		public string Description { get; set; }

		public string Content { get; set; }
	}
}