﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;
using NASK_Zadanie.Models;
using NASK_Zadanie.Services;
using TikaOnDotNet.TextExtraction;

namespace NASK_Zadanie.Controllers
{
	public class HomeController : Controller
	{

		private static readonly List<string> extensions
			= new List<string> { ".doc", ".docx", "odt", ".rtf", ".pdf", ".html" };

		public ActionResult Index(int page = 1, int pageSize = 10, 
			string author = "", string title = "", string description = "", string content = "")
		{
			FileDAO dao = new FileDAO();


			var files = dao.GetFiles();

			// search
			LuceneSearch.AddUpdateLuceneIndex(files);

			// pagination
			ViewBag.Pages = (int)Math.Ceiling((double)(files.Count()) / pageSize);
			var pagedFiles = files
				.Skip((page - 1) * pageSize)
				.Take(pageSize).ToList();
			ViewBag.CurrentPage = page;

			return View(pagedFiles);
		}

		[HttpGet]
		public ActionResult UploadFile()
		{
			return View();
		}

		[HttpPost]
		public ActionResult UploadFile(FileUploadVM model)
		{
			if (model?.File != null && !extensions.Any(ext => model.File.FileName.EndsWith(ext)))
			{
				ModelState.AddModelError("File", "Invalid file extension.");
			}
			if (model != null && !ModelState.IsValid)
			{
				return View(model);
			}

			FileDAO dao = new FileDAO();
			dao.SaveFile(model);

			return RedirectToAction("Index");
		}
	}
}